# Traefik local development proxy

This project contains traefik configuration that I host locally on my laptop
to create easy access for all dockerized services that I'm using.

## Author

Tomasz Maczukin

